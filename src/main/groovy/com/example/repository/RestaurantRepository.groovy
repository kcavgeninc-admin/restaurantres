package com.example.repository

import com.example.domain.BaseEntity
import com.example.domain.Restaurant
import org.springframework.data.jpa.repository.JpaRepository

interface RestaurantRepository extends BaseRepository<Restaurant, Long>, JpaRepository<BaseEntity, Long> {

    Restaurant findByUuidAndRemovedIsFalse(restaurantId)
    Restaurant findByRestaurantNameAndOwnerEmail(String restaurantName, String ownerEmail)
    Restaurant findByCustomer_EmailAddress(String emailAddress)
}
