package com.example.repository

import com.example.domain.BaseEntity
import com.example.domain.Customer
import org.springframework.data.jpa.repository.JpaRepository

interface CustomerRepository extends BaseRepository<Customer, Long>, JpaRepository<BaseEntity, Long> {

    Customer findByEmailAddress(String emailAddress)
    Customer findByUuidAndRemovedIsFalse(String customerId)
}
