package com.example.repository

import com.example.domain.BaseEntity
import com.example.domain.Reservation
import org.springframework.data.jpa.repository.JpaRepository

interface ReservationRepository extends BaseRepository<Reservation, Long>, JpaRepository<BaseEntity, Long> {

    Reservation findByCustomer_EmailAddress(String emailAddress)
    Reservation findByUuidAndRemovedIsFalse(String reservationId)
}
