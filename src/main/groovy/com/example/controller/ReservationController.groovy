package com.example.controller

import com.example.api.ReservationAPI
import com.example.restapi.ReservationCreateResponse
import com.example.restapi.ReservationRequest
import com.example.restapi.ReservationResponse
import com.example.service.ReservationService
import io.swagger.annotations.ApiParam
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
class ReservationController implements ReservationAPI{

    @Autowired
    ReservationService reservationService

    @PostMapping(path = "/reserveTable")
    ReservationCreateResponse reserveTableForCustomerAccount(@RequestBody ReservationRequest request) {
        reservationService.reserveTable(request)
    }

    @PostMapping(path = "/reserveTables")
    List<ReservationCreateResponse> reserveTablesForCustomers(@RequestBody ReservationRequest[] requests) {
        reservationService.reserveTables(requests)
    }

    @GetMapping(path = "/reservation/{emailAddress}")
    ReservationResponse getReservationOfCustomer(@PathVariable(value = "emailAddress", required = true) String emailAddress) {
        reservationService.getReservationOfCustomer(emailAddress)
    }

    @DeleteMapping(path = "/cancelReservation/{emailAddress}/{reservationId}")
    void cancelReservation(@PathVariable(value="reservationId",required = true) String reservationId, @PathVariable(value="emailAddress",required = true) String emailAddress) {
        reservationService.cancelReservation(emailAddress ,reservationId)
    }
}
