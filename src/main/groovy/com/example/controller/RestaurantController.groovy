package com.example.controller

import com.example.api.RestaurantAPI
import com.example.restapi.RestaurantAPIResponse
import com.example.restapi.RestaurantCreateRequest
import com.example.service.RestaurantService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
class RestaurantController implements RestaurantAPI{

    @Autowired
    RestaurantService restaurantService

    @PostMapping(path = "/addRestaurant")
    RestaurantAPIResponse addRestaurantForOwnerAccount(@RequestBody RestaurantCreateRequest request) {
        restaurantService.addRestaurant(request)
    }
}
