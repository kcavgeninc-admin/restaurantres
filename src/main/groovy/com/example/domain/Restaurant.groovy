package com.example.domain

import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.JoinColumn
import javax.persistence.OneToMany
import javax.persistence.OneToOne

@Entity(name="restaurant")
class Restaurant extends BaseEntity{

    @Column
    String restaurantName

    @Column
    String restaurantDescription

    @Column
    String ownerEmail

    @Column
    String restaurantAddress

    @Column
    int totalTables

    @Column
    int maxPerTable

    @Column
    int maxPerCustomer

    @Column
    int totalTablesAvailable

//    @OneToMany(mappedBy = "restaurant", cascade = CascadeType.ALL,fetch= FetchType.EAGER)
//    List<Reservation> reservations

    @OneToOne(fetch= FetchType.LAZY, mappedBy = "restaurant", cascade = CascadeType.ALL)
    Customer customer

    Restaurant(){}

}
