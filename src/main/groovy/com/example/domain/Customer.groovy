package com.example.domain

import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.JoinColumn
import javax.persistence.OneToOne

@Entity(name = "customer")
class Customer extends BaseEntity{

    @Column
    String emailAddress

    @Column
    String userName

    @Column
    String firstName

    @Column
    String lastName

    @OneToOne(fetch= FetchType.EAGER, optional=true)
    @JoinColumn(name="restaurant_id", referencedColumnName = "id")
    Restaurant restaurant

    @OneToOne(fetch= FetchType.EAGER, optional=true, cascade = CascadeType.ALL)
    @JoinColumn(name="reservation_id", referencedColumnName = "id")
    Reservation reservation


}

