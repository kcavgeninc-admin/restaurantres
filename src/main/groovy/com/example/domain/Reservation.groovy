package com.example.domain

import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToOne

@Entity(name = "reservation")
class Reservation extends BaseEntity{
    @Column
    int seatsReservedPerCustomer

    @Column
    String reservationTime

    @OneToOne(fetch= FetchType.LAZY, mappedBy = "reservation")
    Customer customer

//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "restaurant_id")
//    Restaurant restaurant
}
