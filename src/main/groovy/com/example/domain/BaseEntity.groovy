package com.example.domain

import javax.persistence.Column
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.MappedSuperclass
import javax.persistence.PrePersist
import javax.persistence.PreUpdate
import java.time.Instant

import static java.util.UUID.randomUUID

@MappedSuperclass
abstract class BaseEntity {

    @Id
    @Column(unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id

    @Column(unique = true, nullable = false)
    String uuid

    @Column(nullable = false, updatable = false)
    protected Instant created

    @Column
    private Instant updated

    @Column(nullable = false)
    Boolean removed

    Long getId() {
        return id
    }

    String getUuid() {
        return uuid
    }

    Instant getCreated() {
        return created
    }

    Instant getUpdated() {
        return updated
    }

    String uuidPrefix() {
        null
    }

    @PreUpdate
    void onUpdate() {
        updated = Instant.now()
    }

    String generateUuid() {
        if (!uuid) {
            if (uuidPrefix()) {
                uuid = uuidPrefix() + "_" + randomUUID().toString()
            } else {
                uuid = randomUUID().toString()
            }
        }
    }

    @PrePersist
    void onCreate() {

        generateUuid()

        if (removed == null) {
            removed = false
        }

        if (created == null) {
            created = Instant.now()
        }
    }
}
