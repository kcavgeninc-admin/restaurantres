package com.example.restapi

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import io.swagger.annotations.ApiModelProperty

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
class RestaurantAPIResponse {

    @ApiModelProperty(notes = "restaurantIdentifier")
    String uuid

    @ApiModelProperty(notes = "Name of restaurant")
    String restaurantName

    @ApiModelProperty(notes = "Restaurant description")
    String restaurantDescription

    @ApiModelProperty(notes = "owners email address")
    String ownerEmail

    @ApiModelProperty(notes = "Restaurant Address")
    String restaurantAddress

    @ApiModelProperty(notes = "Total number of tables")
    int totalTables

    @ApiModelProperty(notes = "maximum seats per table")
    int maxPerTable

    @ApiModelProperty(notes = "Maximum reservations allowed per customer")
    int maxPerCustomer

    @ApiModelProperty(notes = "Total tables available to be reserved at one time")
    int totalTablesAvailable
}
