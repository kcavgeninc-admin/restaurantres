package com.example.restapi

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import io.swagger.annotations.ApiModelProperty

import javax.persistence.Column

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
class ReservationResponse {

    @ApiModelProperty(notes = "seats reserved by customer")
    int seatsReservedPerCustomer

    @ApiModelProperty(notes = "time of reservation by customer")
    String reservationTime
}
