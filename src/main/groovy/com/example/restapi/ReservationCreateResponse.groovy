package com.example.restapi

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import io.swagger.annotations.ApiModelProperty

import javax.validation.constraints.NotBlank

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
class ReservationCreateResponse {

    @ApiModelProperty(notes = "Reservation Id")
    String uuid

    @ApiModelProperty(notes = "customerIdentifier issued by Auth servers")
    String customerId

    @ApiModelProperty(notes = "Customers Email address")
    String emailAddress

    @ApiModelProperty(notes = "Restaurant Id")
    @NotBlank(message = "{restaurant.restaurantId.required}")
    String restaurantId

    @ApiModelProperty(notes = "Restaurant name")
    String restaurantName

    @ApiModelProperty(notes = "Seats requested to be reserved")
    int seatsRequested

    @ApiModelProperty(notes = "Reservation time")
    String reservationTime

    @ApiModelProperty(notes = "reservation cannot be made for this customer since seats not available")
    String invalidityMessage
}
