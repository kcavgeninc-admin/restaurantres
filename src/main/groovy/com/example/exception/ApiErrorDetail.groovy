package com.example.exception

import com.fasterxml.jackson.annotation.JsonInclude
import io.swagger.annotations.ApiModelProperty

@JsonInclude(JsonInclude.Include.NON_EMPTY)
class ApiErrorDetail {
    @ApiModelProperty(notes = "Source Type")
    String sourceType

    @ApiModelProperty(notes = "Message Id")
    String messageId

    @ApiModelProperty(notes = "Detail")
    String detail

    ApiErrorDetail (String sourceType, String messageId, String detail){
        this.sourceType = sourceType
        this.messageId = messageId
        this.detail = detail
    }


    @Override
    public String toString() {
        return "ApiErrorDetail{" +
                "sourceType='" + sourceType + '\'' +
                ", messageId='" + messageId + '\'' +
                ", detail='" + detail + '\'' +
                '}';
    }
}
