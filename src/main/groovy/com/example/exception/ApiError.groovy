package com.example.exception

import com.fasterxml.jackson.annotation.JsonInclude
import io.swagger.annotations.ApiModelProperty
import org.springframework.http.HttpStatus

import java.time.Instant

@JsonInclude(JsonInclude.Include.NON_EMPTY)
class ApiError {
    @ApiModelProperty(notes = "HTTP Status Code")
    HttpStatus code

    @ApiModelProperty(notes = "Response Timestamp")
    Long timestamp

    @ApiModelProperty(notes = "Error Message")
    String message

    @ApiModelProperty(notes = "Provided X-idempotency-key request header")
    String idempotencyKey

    @ApiModelProperty(notes = "Error Id when error messages are stored or logged with and Id")
    String errrorId

    @ApiModelProperty(notes = "Description related to the error")
    List<ApiError> details

    private ApiError(){
        this.timestamp= Instant.now().getEpochSecond()
    }

    ApiError(HttpStatus status) {
        this.code=status
        this.timestamp= Instant.now().getEpochSecond()
    }


    @Override
    public String toString() {
        return "ApiError{" +
                "code=" + code +
                ", timestamp=" + timestamp +
                ", message='" + message + '\'' +
                ", idempotencyKey='" + idempotencyKey + '\'' +
                ", errrorId='" + errrorId + '\'' +
                ", details=" + details +
                '}';
    }
}

