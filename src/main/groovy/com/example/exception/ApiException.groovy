package com.example.exception

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
class ApiException extends Exception{
    static enum  ErrorType { DATA, CONFLICT, NOTFOUND, SYSTEM, AUTH, UNKNOWN, DECLINED }
    static enum Entity { RESTAURANT, RESERVATION }

    private ErrorType type
    private Entity entity
    private String  historyId

    String idempotencyKey
    String serviceCode

    ApiException(String errorMessage){
        super(errorMessage)
    }

    ApiException(Throwable cause){
        super(cause)
    }

    ApiException(String errorMessage, Throwable cause){
        super(errorMessage, cause);
    }

    ApiException(String errorMessage, ErrorType type){
        super(errorMessage)
        this.type = type
    }

    ApiException(String errorMessage, ErrorType type, Entity entity){
        super(errorMessage)
        this.type = type
        this.entity = entity
    }

    ApiException(String errorMessage, ErrorType type, Entity entity, String historyId){
        super(errorMessage);
        this.type = type;
        this.entity = entity;
        this.historyId = historyId;
    }

    ErrorType getType() {
        return type;
    }

    void setType(ErrorType type) {
        this.type = type;
    }

    Entity getEntity() {
        return entity;
    }

    void setEntity(Entity entity) {
        this.entity = entity;
    }

    String getHistoryId() { return historyId; }

    void setHistoryId(String historyId) { this.historyId = historyId; }
}
