package com.example.service

import com.example.domain.Customer
import com.example.domain.Reservation
import com.example.domain.Restaurant
import com.example.exception.ApiException
import com.example.repository.CustomerRepository
import com.example.repository.ReservationRepository
import com.example.repository.RestaurantRepository
import com.example.restapi.ReservationCreateResponse
import com.example.restapi.ReservationRequest
import com.example.restapi.ReservationResponse
import groovy.util.logging.Slf4j
import org.modelmapper.ModelMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import java.util.concurrent.CompletableFuture

@Slf4j
@Transactional
@Service
class ReservationService extends CommonService{

    @Autowired
    RestaurantRepository restaurantRepository

    @Autowired
    CustomerRepository customerRepository

    @Autowired
    ReservationRepository reservationRepository

    static ModelMapper modelMapper = new ModelMapper()



    def reserveTables(ReservationRequest[] requests) throws ApiException
    {
        def responses = new ArrayList<ReservationCreateResponse> ()

        requests.each {
            responses.add(reserveTable(it))
        }
        responses
    }

    ReservationCreateResponse reserveTable(ReservationRequest request)
    {
        int numberOfSeats = request.seatsRequested
        ReservationCreateResponse reservationAPIResponse = modelMapper.map(request, ReservationCreateResponse.class)
        if(numberOfSeats > 10 )
        {
            reservationAPIResponse.invalidityMessage = "Reservation cannot be made for this customer since total seats requested exceed limit of 10 per customer"
        }
        else {
            Restaurant restaurantFromDB = restaurantRepository.findByUuidAndRemovedIsFalse(request.restaurantId)
            if (!restaurantFromDB)
            {
                reservationAPIResponse.invalidityMessage = "restaurant not found"
            }
            else {
                int totalTablesAvailable = restaurantFromDB.totalTablesAvailable
                if (numberOfSeats>totalTablesAvailable*4)
                {
                    reservationAPIResponse.invalidityMessage = "Restaurant is full and requested seats are not available"
                }
                else{
                    //Great that means we have enough of tables available.  Whats exact number needed for the requested number of seats?
                    Reservation reservation = new Reservation()
                    int seatsBy4 = numberOfSeats/4
                    if (numberOfSeats%4==0)
                    {
                        reservation.seatsReservedPerCustomer = numberOfSeats

                        restaurantFromDB.totalTablesAvailable-= seatsBy4
                    }
                    else{
                        //Handling numberOfSeats which is odd multiple of 4
                        reservation.seatsReservedPerCustomer = numberOfSeats

                        restaurantFromDB.totalTablesAvailable-= seatsBy4 + 1
                    }

                    //Set the date on reservation and save it to db.  Assumption is that date will be sent by client in epoch time to keep it simple
                    reservation.reservationTime = request.reservationTime

                    Customer customer
                    if(request.customerId)
                    {
                        customer = customerRepository.findByUuidAndRemovedIsFalse(request.customerId)
                    }
                    else
                    {
                        customer = customerRepository.findByEmailAddress(request.emailAddress)
                    }

                    if (!customer)
                        customer = new Customer()
                    customer.uuid = request.customerId
                    customer.emailAddress = request.emailAddress
                    customer.reservation = reservation
                    customer.restaurant = restaurantFromDB
                    customerRepository.save(customer)
                    reservationRepository.save(reservation)
                    restaurantRepository.save(restaurantFromDB)

                    reservationAPIResponse = modelMapper.map(request, ReservationCreateResponse.class)
                    reservationAPIResponse.uuid = reservation.uuid
                }
            }
        }

        reservationAPIResponse

    }

    ReservationResponse getReservationOfCustomer(String emailAddress){
        modelMapper.map(reservationRepository.findByCustomer_EmailAddress(emailAddress), ReservationResponse.class)
    }

    void cancelReservation(String emailAddress, String reservationId){
        Reservation reservation = mustExist(RESERVATION_ENTITY, reservationRepository.findByUuidAndRemovedIsFalse(reservationId))
        int seatsReserved = reservation.seatsReservedPerCustomer
        reservationRepository.delete(reservation)

        Restaurant restaurant = restaurantRepository.findByCustomer_EmailAddress(emailAddress)

        int seatsBy4 = seatsReserved/4
        if (seatsReserved%4==0)
        {
            restaurant.totalTablesAvailable+= seatsBy4
        }
        else{
            restaurant.totalTablesAvailable+= seatsBy4 + 1
        }
        restaurantRepository.save(restaurant)

    }

}
