package com.example.service

import com.example.exception.ApiException

class CommonService {

    protected static final String CUSTOMER_ENTITY = 'CUSTOMER'
    protected static final String RESTAURANT_ENTITY = 'RESTAURANT'
    protected static final String RESERVATION_ENTITY = 'RESERVATION'


    protected  <T> T mustExist(final T entity) {
        if(null == entity) {
            throw new ApiException(entity.class.name+" not found", ApiException.ErrorType.DATA)
        }
        return entity

    }

    protected  <T> T mustBeEqual(final T expected, final T actual, final String message) {
        if(null == expected) {
            throw new ApiException(expected.class.name+" not found", ApiException.ErrorType.NOTFOUND)
        }
        if(null == actual) {
            throw new ApiException(actual.class.name+" not found", ApiException.ErrorType.NOTFOUND)
        }
        if(expected != actual) {
            throw new ApiException(message, ApiException.ErrorType.CONFLICT)
        }
        return expected

    }

    protected  <T> T mustExist(final String entityName, final T entity) {
        if(!entity) {
            throw new ApiException(entityName+" not found", ApiException.ErrorType.NOTFOUND)
        }
        return entity

    }

    protected  <T> T mustExist(final String entityName, final T entity, String idempotencyKey) {
        if(!entity) {
            throw new ApiException(entityName+" not found", idempotencyKey, ApiException.ErrorType.NOTFOUND)
        }
        return entity

    }

    protected <T> void mustNotExist(final T entity) {
        if(entity) {
            throw new ApiException(entity.class.name+" already exists", ApiException.ErrorType.CONFLICT)
        }
    }

    protected <T> void mustNotExist(final String entityName, final T entity) {
        if(entity) {
            throw new ApiException(entityName+" already exists", ApiException.ErrorType.CONFLICT,ApiException.Entity.valueOf(entityName))
        }
    }
}
