package com.example.service

import com.example.domain.Restaurant
import com.example.repository.RestaurantRepository
import com.example.restapi.RestaurantAPIResponse
import com.example.restapi.RestaurantCreateRequest
import groovy.util.logging.Slf4j
import org.modelmapper.ModelMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Slf4j
@Transactional
@Service
class RestaurantService extends CommonService{

    @Autowired
    RestaurantRepository restaurantRepository

    static ModelMapper modelMapper = new ModelMapper()

    RestaurantAPIResponse addRestaurant(RestaurantCreateRequest request)
    {
        Restaurant restaurant = restaurantRepository.findByRestaurantNameAndOwnerEmail(request.restaurantName, request.ownerEmail)
        if (!restaurant)
        {
            restaurant = modelMapper.map(request, Restaurant.class)
            restaurantRepository.save(restaurant)
        }
        RestaurantAPIResponse restaurantAPIResponse = modelMapper.map(restaurant, RestaurantAPIResponse.class)
        restaurantAPIResponse
    }

}
