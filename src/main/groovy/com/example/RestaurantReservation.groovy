package com.example

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class RestaurantReservation {
     static void main(String[] args) {
        SpringApplication.run(RestaurantReservation.class, args)
    }
}
