package com.example.api

import com.example.exception.ApiError
import com.example.restapi.RestaurantAPIResponse
import com.example.restapi.RestaurantCreateRequest
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses

@Api(value = "Restaurant", tags = ["Restaurant"], produces = "application/json", consumes = "application/json")
interface RestaurantAPI {

    @ApiOperation(value = "Add a new restaurant", tags = ["Restaurant"])
    @ApiResponses(
            [
                    @ApiResponse(code = 201, message = "New Restaurant added to db"),
                    @ApiResponse(code = 400, message = "User or Data error", response = ApiError),
            ]
    )
    RestaurantAPIResponse addRestaurantForOwnerAccount(RestaurantCreateRequest request)

}
