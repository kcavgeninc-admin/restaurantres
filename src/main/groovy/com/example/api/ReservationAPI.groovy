package com.example.api

import com.example.exception.ApiError
import com.example.restapi.ReservationCreateResponse
import com.example.restapi.ReservationRequest
import com.example.restapi.ReservationResponse
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses

@Api(value = "Reservation", tags = ["Reservation"], produces = "application/json", consumes = "application/json")
interface ReservationAPI {

    @ApiOperation(value = "Reserve a table", tags = ["Reservation"])
    @ApiResponses(
            [
                    @ApiResponse(code = 201, message = "Table reserved by a Customer or Owner"),
                    @ApiResponse(code = 400, message = "User or Data error", response = ApiError),
            ]
    )
    ReservationCreateResponse reserveTableForCustomerAccount(ReservationRequest request)

    @ApiOperation(value = "Get reservation of a customer", tags = ["Reservation"])
    @ApiResponses(
            [
                    @ApiResponse(code = 200, message = "Success"),
                    @ApiResponse(code = 404, message = "Reservation Not Found", response = ApiError)
            ]
    )
    ReservationResponse getReservationOfCustomer(@ApiParam(value = "emailAddress", required = true) String emailAddress)

    @ApiOperation(value = "Cancel a reservation of a Customer", tags = ["Reservation"])
    @ApiResponses(
            [
                    @ApiResponse(code = 201, message = "Reservation cancelled by Customer"),
                    @ApiResponse(code = 400, message = "User or Data error", response = ApiError),
            ]
    )
    void cancelReservation(@ApiParam(value="reservationId",required = true) String reservationId, @ApiParam(value="emailAddress",required = true) String emailAddress)

}
