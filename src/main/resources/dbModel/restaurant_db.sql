create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, customer_id int8, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FK41v6ueo0hiran65w8y1cta2c2 foreign key (customer_id) references customer
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, customer_id int8, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
alter table if exists restaurant add constraint FK1v2vih2s2rkec408vn4ttkipc foreign key (customer_id) references customer
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, restaurant_id int8, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
alter table if exists reservation add constraint FKbna4xjm3tqln2j6kda3fl2yiy foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
create table customer (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, email_address varchar(255), first_name varchar(255), last_name varchar(255), user_name varchar(255), reservation_id int8, restaurant_id int8, primary key (id))
create table reservation (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, reservation_time varchar(255), seats_reserved_per_customer int4, primary key (id))
create table restaurant (id  bigserial not null, created timestamp not null, removed boolean not null, updated timestamp, uuid varchar(255) not null, max_per_customer int4, max_per_table int4, owner_email varchar(255), restaurant_address varchar(255), restaurant_description varchar(255), restaurant_name varchar(255), total_tables int4, total_tables_available int4, primary key (id))
alter table if exists customer add constraint UK_q8w2f8xfdoax44qc8w0epholu unique (uuid)
alter table if exists reservation add constraint UK_moiq4g356jpprp53nr38nec8r unique (uuid)
alter table if exists restaurant add constraint UK_ikgu0ii4pjoivgawm8adfa07u unique (uuid)
alter table if exists customer add constraint FKfhkie0tx2f7v87wsvxadmnr96 foreign key (reservation_id) references reservation
alter table if exists customer add constraint FK8urw76ju2mbxlssauuwe47mte foreign key (restaurant_id) references restaurant
