FROM java:8

WORKDIR /

COPY build/libs/RestaurantReservation-1.0-SNAPSHOT.jar /RestaurantReservation-1.0-SNAPSHOT.jar

EXPOSE 9000
CMD java - jar RestaurantReservation-1.0-SNAPSHOT.jar

